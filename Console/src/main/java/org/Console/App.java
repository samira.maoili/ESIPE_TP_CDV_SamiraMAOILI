package org.Console;

import java.util.Scanner;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.Business.Fib;

public class App {

	static final Logger logger = Logger.getLogger(App.class);

	public static void main(String[] args) {
		System.out.println("saisissez un chiffre: ");
		String str = new Scanner(System.in).nextLine();
		int n;
		if(str.length()>0 )
			n = Integer.parseInt(str);
		else
			n=5;
		BasicConfigurator.configure();
		logger.debug(str);
		for (int i = 1 ; i <= n ; i++ ) {
			System.out.print(Fib.f(i)+"\t");
			logger.debug(Fib.f(i)+"\t");
		}

	}

}

