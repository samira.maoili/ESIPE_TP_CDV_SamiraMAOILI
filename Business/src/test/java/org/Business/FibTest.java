package org.Business;

import junit.framework.TestCase;
import static org.junit.Assert.*;
import org.junit.Test;
import java.lang.annotation.Annotation;

/**
 * Unit test for simple Fib.
 */
public class FibTest extends TestCase{
	/**
	 * Create the test case
	 *
	 * @param testName name of the test case
	 */
	public FibTest( String testName )
	{
		super( testName );
	}


	@Test
	public void testFibAvecTrue()
	{
		assertTrue(1==Fib.f(1));
		assertTrue(0==Fib.f(0));
		assertTrue(1==Fib.f(2));
		assertTrue(13==Fib.f(7));
		System.out.println("test avec assertTrue OK");
	}
	
	@Test
	public void testFibAvecEquals()
	{
		assertEquals(1, Fib.f(1));
		assertEquals(0, Fib.f(0));
		assertEquals(1, Fib.f(2));
		assertEquals(13, Fib.f(7));
		System.out.println("test avec assertEquals OK");
	}
}
